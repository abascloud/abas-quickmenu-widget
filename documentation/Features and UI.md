# abas-quickmenu-widget Features and visible UI-components

The abas-quickmenu-widget can be used to create a menu of buttons which may initiate several different clickactions.

## Images

![Alt text](Example.jpg "Widget button menu with 4 buttons.")

## UI-Components

| Component | Display Condition
| --- | --- |
| Title | - |
| Entries | Any entries configured in the settings dialog are displayed.  |
| Entry Icon | Displayed if icon selected in the settings dialog and enough space to render. |
| Entry Text | Displayed if text entered in the settings dialog and enough space to render as is or when truncated. |

## Features

| Feature / Test ID  | Trigger | Description | Prerequisite  |
| --- | --- | --- | --- |
| Update Board | Click on widget | A message for the preconfigured field / value combinations is dispatched. | *update Board* clickaction selected in settings dialog |
| Open ERP Command | Click on widget | The preconfigured ERP command is executed. | *abas ERP* clickaction selected in settings dialog |
| Open Dashboard | Click on widget | The preconfigured target Dashboard is opened. | *dashboard* clickaction selected in settings dialog |
| Open external link | Click on widget | The preconfigured external link is opened. | *external Link* clickaction selected in settings dialog |

## Settings Dialog

![Alt text](Settings.jpg "Settings Dialog")

| Settings Title | Description | Input Type | Display Condition | Settings Section |
| --- | --- | --- | --- | --- |
| Title | Enter the widget title. | String | - |General |
| abas-ERP client | Select an available ERP client. | Dropdown | - |General |
| fixed | Lock the selected ERP client? | Checkbox | - | General |
| Add | Add a row to the Subscription-Table. | Button | - | Subscriptions |
| Subscription-Table | Subscription-Table with fields and databases. | See details |  If any rows exist in the table  | Subscriptions |
| Add Menu Entry | Add a button to the menu displayed by the widget. | Button | - | Tiles |
| Entries table | Displays the menu entries. | See details | - | Tiles |

### Settings Entries table

![Alt text](SettingsEntry.jpg "Settings Dialog: Entries table")

| Column | Description | Input Type | Display Condition |
| --- | --- | --- | --- |
| Click action | Choose a clickaction to be executed if the widget is clicked on. | String | - | 
| Click action | Display settings for individual clickaction-type. | See details | If fitting action selected | 
| Label | Enter a label to be displayed by the widget. | String | - |
| Icon | Displays the currently selected icon. | - | - |
| Icon-Add-Button | Click displays the Icon Selection. | Button | If no icon currently selected | 
| Icon-Edit-Button | Click displays the Icon Selection. | Button | If any icon currently selected | 
| Icon-Delete-Button | Click clears the currently selected icon. | Button | If any icon currently selected | 
| Color | Select a color through a color picker. | Button |  - |
| - | Click on delete button removes the entry. | Button | - |


| Setting Component | Description | Input Type | Display Condition |
| --- | --- | --- | --- |
| Color-Picker | Select the intended color. | See details |  If Color-Button clicked and color picker not deselected |
| Icon Selection | Displays available Icons | See details | Icon-Edit-Button or Icon-Add-Button clicked and Icon Selection not closed |

### Icon Selection Details

![Alt text](SettingsButton.jpg "Details Button Settings")

| Component | Description | Input Type |
| --- | --- | --- |
| Back Button | Click closes the *Icon Selection* without changes. | Button | 
| Icons with name | If clicked on the icon is chosen to be displayed by the widget and the *Icon Selection* is closed. | Button | --- |

### Color-Picker Details

![Alt text](SettingsColor.jpg "Details Color Settings")

| Title | Description | Input Type |
| --- | --- | --- |
| Click outside | Closes the *Color-Picker* without any changes. | Click | 
| Color Button | Click on the button selects the color to be displayed by the widget and closes the *Color-Picker*. | Button |


### Subscription-Table Details

![Alt text](SettingsSubscriptions.jpg "Details Subscription Settings")

| Column Name | Description | Input Type |
| --- | --- | --- |
| Field name | Determines the field the widget subscribes to. | String | 
| Topics | Determines the database the widget subscribes to. | Dropdown | 
| - | Delete the row from the Table. | Delete-Icon | 

## Click Action Options Details

#### Click Action: Update Dashboard

![Alt text](SettingsClickUpdate.jpg "Details Clickaction Update")

Add row:

| Title | Description | Input Type |
| --- | --- | --- |
| Field name | Name of the field for the clickaction. | String |
| Field value | Value of the field for the clickaction.  | String |
| Add Button | Click adds the field name / value combination to the clickaction. | Button |

Table:

| Column Name | Description | Input Type |
| --- | --- | --- |
| Field name | Displays the field name. | - | 
| Field value | Displays the field value. | - | 
| - | Delete the row from the Table. | Delete-Button | 

#### Click Action: ERP

![Alt text](SettingsClickERP.jpg "Details Clickaction ERP")

| Component | Description | Input Type |
| --- | --- | --- |
| ERP command | Name of the field for the clickaction. | String |
| Settings Icon | Opens the ERP Command Builder Dialog. | Button |

![Alt text](SettingsClickERP2.jpg "Details Clickaction ERP Command Builder Database")

| Name | Description | Input Type | Section |
| --- | --- | --- | --- |
| Type | Type of the ERP command. | Dropdown | Command Builder
| Database | Choose a Database. | Dropdown | Command Builder
| Action | Choose action for the command. | Dropdown | Command Builder
| Database object | Identifier for a single database entry. | String | Command Builder
| Screen actions | Enter any screen actions. | String | Command Builder
| Generate command | Build the command from settings in *Command Builder* section. Display in *ERP command* field. | Button | Command Builder
| ERP command | Command build by the command builder section. May be manually modified. | String | Result
| Try out | Run the command given in *ERP command* | Button | Result
| Subscription Table | Displays subscription topics and field names. Click on *Plus* icon adds the subscription to the *Database object* string. | Button | Available Subscriptions
| OK | Click closes the Command Builder and applies the chosen command. | Button | -

![Alt text](SettingsClickERP3.jpg "Details Clickaction ERP Command Builder Infosystem / Type Command")

| Name | Description | Input Type | Section |
| --- | --- | --- | --- |
| Type | Type of the ERP command. | Dropdown | Command Builder
| Type Command / Infosystem | Choose a Type Command or Infosystem. | Dropdown | Command Builder
| Start Immediately | Infosystem only; Run the command immediately? | Checkbox | Command Builder
| Query | Additional query string for the command. | String | Command Builder
| Generate command | Build the command from settings in *Command Builder* section. Display in *ERP command* field. | Button | Command Builder
| ERP command | Command build by the command builder section. May be manually modified. | String | Result
| Try out | Run the command given in *ERP command* | Button | Result
| Subscription Table | Displays subscription topics and field names. Click on *Plus* icon adds the subscription to the query string. | Button | Available Subscriptions
| OK | Click closes the Command Builder and applies the chosen command. | Button | -

#### Click Action: External

![Alt text](SettingsClickExternal.jpg "Details Clickaction External")

| Title | Description | Input Type |
| --- | --- | --- |
| External Link | Enter URL of target web resource. | String |

#### Click Action: Dashboard

![Alt text](SettingsClickBoard.jpg "Details Clickaction Dashboard")

| Title | Description | Input Type |
| --- | --- | --- |
| Dashboard | Select Dashboard from the list of available Dashboards. | Dropdown |