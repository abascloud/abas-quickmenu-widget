# 1.0.10

- Some cleanup to minimize the stored data

# 1.0.9

- Subscriptions can now be used in erp commands

# 1.0.8

- Corrected some design issues.

# 1.0.7

- Corrected the message which is sent when you click on an "update board" tile. The used topic is "clickaction" now.

# 1.0.6

- Dashboards will now be opened in the same tab ("_self") instead of a new one.

# 1.0.5

- Rewritten the quickmenu widget using abas-elements.
You now have not only the possibility to define a abas ERP command per tile, but also can define other click actions just like in the other widgets, e.g. open another dashboard, an external link or sending informations to the message bus.
